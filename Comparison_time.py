import yaml
import matplotlib.pyplot as plt
# Import classes of databases
from Mongo import Mongo
from ClickHouse import MyClickHouse
from PosgreSQL import PostgreSQL
from MySQL import MySQL

# Open the file with configurations
with open('conf.yml', 'r') as yamlfile:
    config = yaml.safe_load(yamlfile)


# This class compare time completing insert, select and delete functions
class ComparisonTime:
    def __init__(self):
        self.test_mongo = Mongo('mongodb://einvogel:1111@0.0.0.0:2717', 'test', 'Test')
        self.test_clickhouse = MyClickHouse('http://localhost:8123')
        self.test_postgres = PostgreSQL('test', 'einvogel', 1111, '0.0.0.0', 5433)
        self.test_mysql = MySQL('0.0.0.0', 'einvogel', '1111', 'db')

    def comparison_time_insert_in_turn(self):
        # Create tables in ClickHouse, Postges, MySQL. In MongoDB you don't need do it,
        # because collection create at time insert
        self.test_clickhouse.create_table()
        self.test_postgres.create_table()
        self.test_mysql.create_table()
        # Insert data
        insert_time_mongo = self.test_mongo.insert_in_turn()
        insert_time_clcikhouse = self.test_clickhouse.insert_it_turn()
        insert_time_postgres = self.test_postgres.insert_in_turn()
        insert_time_mysql = self.test_mysql.insert_in_turn()
        numbers_databases = [1, 2, 3, 4]
        names_databases = ['Mongo', 'ClickHouse', 'Postgres', 'Mysql']
        time_each_insert = [insert_time_mongo, insert_time_clcikhouse, insert_time_postgres, insert_time_mysql]
        # Create an heading on the diagram
        plt.title('Time to insert data in turn')
        plt.xticks(numbers_databases, names_databases)
        plt.bar(numbers_databases, time_each_insert)
        plt.show()

    def comparison_time_insert_batch(self):
        # Create tables in ClickHouse, Postges, MySQL. In MongoDB you don't need do it,
        # because collection create at time insert
        self.test_clickhouse.create_table()
        self.test_postgres.create_table()
        self.test_mysql.create_table()
        # Insert data with a batch size
        insert_time_mongo = self.test_mongo.insert_batch()
        insert_time_clcikhouse = self.test_clickhouse.insert_batch()
        insert_time_postgres = self.test_postgres.insert_batch()
        insert_time_mysql = self.test_mysql.insert_batch()
        numbers_databases = [1, 2, 3, 4]
        names_databases = ['Mongo', 'ClickHouse', 'Postgres', 'Mysql']
        time_each_insert = [insert_time_mongo, insert_time_clcikhouse, insert_time_postgres, insert_time_mysql]
        # Create an heading on the diagram
        plt.title('Time to insert batch of data')
        plt.xticks(numbers_databases, names_databases)
        plt.bar(numbers_databases, time_each_insert)
        plt.show()

    def comparison_time_select(self):
        # Select data
        select_result_mongo, select_time_mongo = self.test_mongo.select()
        select_result_clickhouse, select_time_clcikhouse = self.test_clickhouse.select()
        select_result_postgres, select_time_postgres = self.test_postgres.select()
        select_result_mysql, select_time_mysql = self.test_mysql.select()
        numbers_databases = [1, 2, 3, 4]
        names_databases = ['Mongo', 'ClickHouse', 'Postgres', 'Mysql']
        time_each_insert = [select_time_mongo, select_time_clcikhouse, select_time_postgres, select_time_mysql]
        # Create an heading on the diagram
        plt.title('Time to select whole row of data')
        plt.xticks(numbers_databases, names_databases)
        plt.bar(numbers_databases, time_each_insert)
        plt.show()

    def comparison_time_update(self):
        # Update data
        update_time_mongo = self.test_mongo.update()
        # ClickHouse doesn't have a function 'update'
        update_time_postgres = self.test_postgres.update()
        update_time_mysql = self.test_mysql.update()
        numbers_databases = [1, 2, 3]
        names_databases = ['Mongo', 'Postgres', 'Mysql']
        time_each_insert = [update_time_mongo, update_time_postgres, update_time_mysql]
        # Create an heading on the diagram
        plt.title('Time to update 1 element of row')
        plt.xticks(numbers_databases, names_databases)
        plt.bar(numbers_databases, time_each_insert)
        plt.show()

    def comparison_time_delete(self):
        # Insert data with a batch size
        delete_time_mongo = self.test_mongo.drop_collection()
        delete_time_clcikhouse = self.test_clickhouse.drop_table()
        delete_time_postgres = self.test_postgres.drop_table()
        delete_time_mysql = self.test_mysql.drop_table()
        numbers_databases = [1, 2, 3, 4]
        names_databases = ['Mongo', 'ClickHouse', 'Postgres', 'Mysql']
        time_each_insert = [delete_time_mongo, delete_time_clcikhouse, delete_time_postgres, delete_time_mysql]
        # Create an heading on the diagram
        plt.title('Time to delete data')
        plt.xticks(numbers_databases, names_databases)
        plt.bar(numbers_databases, time_each_insert)
        plt.show()

    def drop_all_table(self):
        self.test_mongo.drop_collection()
        self.test_clickhouse.drop_table()
        self.test_postgres.drop_table()
        self.test_mysql.drop_table()