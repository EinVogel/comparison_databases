import time
import yaml
import psycopg2

with open('conf.yml', 'r') as yamlfile:
    config = yaml.safe_load(yamlfile)


class PostgreSQL:
    def __init__(self, db_name, db_user, db_pass, db_host, db_port):
        self.db_name = db_name
        self.db_user = db_user
        self.db_pass = db_pass
        self.db_host = db_host
        self.db_port = db_port
        self.connection = psycopg2.connect(database=self.db_name, user=self.db_user, password=self.db_pass,
                                           host=self.db_host, port=self.db_port)
        self.cursor = self.connection.cursor()

    def create_table(self):
        self.cursor.execute("CREATE TABLE Test (ID INT NOT NULL, "
                            "NAME CHAR(20), AGE INT, SEX CHAR(1), NUMBER CHAR(20))")
        print("PostgreSQL - the table successfully created")
        self.connection.commit()

    def insert_in_turn(self):
        id_row = 0
        start = time.time()
        for note in range(0, 10):
            self.cursor.execute("INSERT INTO Test(ID, NAME, AGE, SEX, NUMBER)"
                                " VALUES ({id}, 'test', 20, 'M', '88005553535')".format(id=id_row))
            id_row += 1
        finish = time.time()
        insert_time_postgres = finish - start
        print("Time to insert data to PostgreSQL is", insert_time_postgres, 'sec')
        self.connection.commit()
        return insert_time_postgres

    def insert_batch(self):
        batch_size = config['batch_size']
        list_of_rows = [(id_row, 'test', 20, 'M', '88005553535') for id_row in range(batch_size)]
        # take all rows and concatenate to one line
        rows_to_line = ''
        for row in list_of_rows:
            rows_to_line = rows_to_line + str(row) + ', '
        # Remove last ',\t'
        rows_to_line = rows_to_line[:-2]
        start = time.time()
        self.cursor.execute(("INSERT INTO Test(ID, NAME, AGE, SEX, NUMBER)"
                             " VALUES {dataset}".format(dataset=rows_to_line)))
        finish = time.time()
        insert_time_postgres = finish - start
        print("Time to insert batch_size", batch_size, "of data to PostgreSQL is", insert_time_postgres, 'sec')
        self.connection.commit()
        return insert_time_postgres

    def select(self):
        start = time.time()
        self.cursor.execute(config['postgres_select_query'])
        results = self.cursor.fetchall()
        finish = time.time()
        select_time_postgres = finish - start
        print("Time to select data from PostgreSQL is", select_time_postgres, 'sec')
        self.connection.commit()
        return results, select_time_postgres

    def update(self):
        start = time.time()
        self.cursor.execute(config['postgres_update_query'])
        finish = time.time()
        update_time_postgres = finish - start
        print("Time to update data in PostgreSQL is", update_time_postgres, 'sec')
        self.connection.commit()
        return update_time_postgres

    def drop_table(self):
        start = time.time()
        self.cursor.execute("DROP TABLE Test")
        finish = time.time()
        delete_time_postgres = finish - start
        # Commit is an operation, which gives a green signal to the database to finalize the changes
        self.connection.commit()
        print("PostgreSQL - the table successfully deleted")
        print("Time to delete data from PostgreSQL is", delete_time_postgres, 'sec')
        return delete_time_postgres