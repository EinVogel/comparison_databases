import time
import yaml
import requests

with open('conf.yml', 'r') as yamlfile:
    config = yaml.safe_load(yamlfile)


class MyClickHouse:
    def __init__(self, url):
        self.connection = url

    def create_table(self):
        requests.post(self.connection, 'CREATE TABLE Test (ID Int64, NAME String, AGE Int8,'
                                       ' SEX String, NUMBER String) ENGINE=MergeTree()'
                                       ' [PARTITION BY ID] [ORDER BY ID] [PRIMARY KEY ID]')
        print("ClickHouse - the table successfully created")

    def insert_it_turn(self):
        id_row = 0
        start = time.time()
        for note in range(0, 10):
            requests.post(self.connection, "INSERT INTO Test(ID, NAME, AGE, SEX, NUMBER)"
                                           " VALUES ({id}, 'test', 20, 'M', '88005553535')".format(id=id_row))
            id_row += 1
        finish = time.time()
        insert_time_clickhouse = finish - start
        print("Time to insert data to Clickhouse is", insert_time_clickhouse, 'sec')
        return insert_time_clickhouse

    def insert_batch(self):
        batch_size = config['batch_size']
        list_of_rows = [(id_row, 'test', 20, 'M', '88005553535') for id_row in range(batch_size)]
        # take all rows and concatenate to one line
        rows_to_line = ''
        for row in list_of_rows:
            rows_to_line = rows_to_line + str(row) + ', '
        # Remove last ',\t'
        rows_to_line = rows_to_line[:-2]
        start = time.time()
        requests.post(self.connection, "INSERT INTO Test(ID, NAME, AGE, SEX, NUMBER)"
                                       " VALUES {dataset}".format(dataset=rows_to_line))
        finish = time.time()
        insert_time_clickhouse = finish - start
        print("Time to insert batch_size", batch_size, "of data to Clickhouse is", insert_time_clickhouse, 'sec')
        return insert_time_clickhouse

    def select(self):
        start = time.time()
        result = requests.post(self.connection, config['clickhouse_select_query'])
        finish = time.time()
        select_time_clickhouse = finish - start
        print("Time to select data from ClickHouse is", '{:0.9f}'.format(select_time_clickhouse), 'sec')
        return result, select_time_clickhouse

    def drop_table(self):
        start = time.time()
        requests.post(self.connection, 'DROP TABLE Test')
        finish = time.time()
        delete_time_clickhouse = finish - start
        print("ClickHouse - the table successfully deleted")
        print("Time to select data from ClickHouse is", '{:0.9f}'.format(delete_time_clickhouse), 'sec')
        return delete_time_clickhouse