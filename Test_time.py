from Comparison_time import ComparisonTime

# Initializing class instance
comparison_insert_time = ComparisonTime()

comparison_insert_time.comparison_time_insert_in_turn()
comparison_insert_time.drop_all_table()
comparison_insert_time.comparison_time_insert_batch()
comparison_insert_time.comparison_time_update()
comparison_insert_time.comparison_time_select()
comparison_insert_time.comparison_time_delete()
