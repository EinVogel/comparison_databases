import time
import yaml
from pymongo import MongoClient

with open('conf.yml', 'r') as yamlfile:
    config = yaml.safe_load(yamlfile)


# Class database MongoDB.
class Mongo:
    # Initializing parameters of database
    def __init__(self, address, database_name, collection_name):
        self.address = address
        self.cluster = MongoClient(address)
        self.db = self.cluster[database_name]
        self.collection = self.db[collection_name]

    # Insert data to collection in turn. 1 row at 1 time. Not batch
    def insert_in_turn(self):
        id_row = 0
        start = time.time()
        for note in range(0, 10):
            self.collection.insert_one({'_id': id_row, 'name': 'test', 'age': 20, 'sex': 'M', 'number': '88005553535'})
            id_row += 1
        finish = time.time()
        # Take time to insert data to database
        insert_time_mongo = finish - start
        print("Time to insert data to MongoDB is", insert_time_mongo, 'sec')
        return insert_time_mongo

    # Insert data to collection in turn. 1 row at 1 time. Not batch
    def insert_batch(self):
        # Size your quantity documents which you want to insert to database
        batch_size = config['batch_size']
        list_of_documents = [{'_id': id_row, 'name': 'test', 'age': 20, 'sex': 'M', 'number': '88005553535'}
                             for id_row in range(batch_size)]
        start = time.time()
        self.collection.insert_many(list_of_documents)
        finish = time.time()
        # Take time to insert data to database
        insert_time_mongo = finish - start
        print("Time to insert batch_size", batch_size, "of data to MongoDB is", insert_time_mongo, 'sec')
        return insert_time_mongo

    # Select whole row
    def select(self):
        start = time.time()
        result = self.collection.find(config['mongo_select_query'])
        finish = time.time()
        # Take time to select data from database
        select_time_mongo = finish - start
        print("Time to select data from MongoDB is", '{:0.9f}'.format(select_time_mongo), 'sec')
        return result, select_time_mongo

    def update(self):
        start = time.time()
        self.collection.update(config['mongo_update_query_part_1'], config['mongo_update_query_part_2'])
        finish = time.time()
        # Take time to update data in database
        update_time_mongo = finish - start
        print("Time to update data in MongoDB is", '{:0.9f}'.format(update_time_mongo), 'sec')
        return update_time_mongo

    # Drop collection
    def drop_collection(self):
        start = time.time()
        self.collection.drop()
        finish = time.time()
        delete_time_mongo = finish - start
        print("MongoDB - the collection successfully deleted")
        print("Time to delete data from MongoDB is", '{:0.9f}'.format(delete_time_mongo), 'sec')
        return delete_time_mongo
