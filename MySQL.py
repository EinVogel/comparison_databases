import time
import yaml
import pymysql

with open('conf.yml', 'r') as yamlfile:
    config = yaml.safe_load(yamlfile)


class MySQL:
    def __init__(self, db_host, db_user, db_pass, db_name):
        self.db_host = db_host
        self.db_user = db_user
        self.db_pass = db_pass
        self.db_name = db_name
        self.connection = pymysql.connect(host=self.db_host, user=self.db_user, password=self.db_pass, db=self.db_name, charset='utf8mb4')
        self.cursor = self.connection.cursor()

    def create_table(self):
        self.cursor.execute("CREATE TABLE Test (ID INT NOT NULL, "
                            "NAME CHAR(20), AGE INT, SEX CHAR(1), NUMBER CHAR(20))")
        print("MySQL - the table successfully created")
        self.connection.commit()

    def insert_in_turn(self):
        id_row = 0
        start = time.time()
        for note in range(0, 10):
            self.cursor.execute("INSERT INTO Test(ID, NAME, AGE, SEX, NUMBER)"
                                " VALUES ({id}, 'test', 20, 'M', '88005553535')".format(id=id_row))
            id_row += 1
        finish = time.time()
        insert_time_mysql = finish - start
        print("Time to insert data to MySQL is", insert_time_mysql, 'sec')
        self.connection.commit()
        return insert_time_mysql

    def insert_batch(self):
        batch_size = config['batch_size']
        list_of_rows = [(id_row, 'test', 20, 'M', '88005553535') for id_row in range(batch_size)]
        # take all rows and concatenate to one line
        rows_to_line = ''
        for row in list_of_rows:
            rows_to_line = rows_to_line + str(row) + ', '
        # Remove last ',\t'
        rows_to_line = rows_to_line[:-2]
        start = time.time()
        self.cursor.execute("INSERT INTO Test(ID, NAME, AGE, SEX, NUMBER)"
                            " VALUES {dataset}".format(dataset=rows_to_line))
        finish = time.time()
        insert_time_mysql = finish - start
        print("Time to insert batch_size", batch_size, "of data to MySQL is", insert_time_mysql, 'sec')
        self.connection.commit()
        return insert_time_mysql

    def select(self):
        start = time.time()
        self.cursor.execute(config['mysql_select_query'])
        result = self.cursor.fetchall()
        finish = time.time()
        select_time_mysql = finish - start
        print("Time to select data from MySQL is", select_time_mysql, 'sec')
        self.connection.commit()
        return result, select_time_mysql

    def update(self):
        start = time.time()
        self.cursor.execute(config['mysql_update_query'])
        finish = time.time()
        update_time_mysql = finish - start
        print("Time to update data in MySQL is", update_time_mysql, 'sec')
        self.connection.commit()
        return update_time_mysql

    def drop_table(self):
        start = time.time()
        self.cursor.execute("DROP TABLE Test")
        finish = time.time()
        delete_time_mysql = finish - start
        # Commit is an operation, which gives a green signal to the database to finalize the changes
        self.connection.commit()
        print("MySQL - the table successfully deleted")
        print("Time to update data in MySQL is", delete_time_mysql, 'sec')
        return delete_time_mysql